﻿using System.Collections.Generic;
using System.Linq;
using AutoMapperPerformance;
using AutoMapperPerformance.Mappers;
using AutoMapperPerformance.Objects;
using NUnit.Framework;

namespace AutoMapperPerformanceTests
{
    [TestFixture]
    public class ValueTypeAutomapperMapperTests
    {
        private const int NumberOfInputElements = 1;

        private IList<ValueTypeVariablesSmallObjectA> _smallAObjects;
        private ValueTypeAutomapperMapper SUT;

        [OneTimeSetUp]
        public void Setup()
        {
            _smallAObjects = ObjectGenerator.GenerateCollectionOfObjects<ValueTypeVariablesSmallObjectA>(NumberOfInputElements);
            SUT = new ValueTypeAutomapperMapper(_smallAObjects);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            SUT.ResetMapper();
        }

        [Test]
        public void CheckIfCountOfInputElementEqualsOutput([Random(1, 100, 1)] int numberOfInputElements)
        {
            // Assign
            _smallAObjects = ObjectGenerator.GenerateCollectionOfObjects<ValueTypeVariablesSmallObjectA>(NumberOfInputElements);

            // Act
            var result = SUT.ToObjectsB();

            // Assert
            Assert.AreEqual(_smallAObjects.Count, result.Count);
        }

        [Test]
        public void CheckIfTypeOfOutputIsCorrect_InputValueTypeVariablesSmallObjectA_OutputValueTypeVariablesSmallObjectB()
        {
            // Assign
            var _smallAObjects = ObjectGenerator.GenerateCollectionOfObjects<ValueTypeVariablesSmallObjectA>(1);

            // Act
            var result = SUT.ToObjectsB();

            // Assert
            Assert.IsNotInstanceOf<List<ValueTypeVariablesSmallObjectA>>(result);
            Assert.IsInstanceOf<List<ValueTypeVariablesSmallObjectB>>(result);
        }

        [Test]
        public void CheckIfChangeInObjectACauseChangeInObjectB()
        {
            // Assign
            var inputObjects = _smallAObjects;

            // Act
            var result = SUT.ToObjectsB();
            inputObjects[0].Number = 123;


            // Assert
            Assert.AreNotEqual(inputObjects[0].Number, result.First(o => o.Id == inputObjects[0].Id).Number);
        }
    }
}
