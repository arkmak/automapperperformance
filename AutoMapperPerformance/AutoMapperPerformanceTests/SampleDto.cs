﻿namespace AutoMapperPerformanceTests
{
    public class SampleDto
    {
        public long Id { get; set; }
        public int Number { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
    }
}
