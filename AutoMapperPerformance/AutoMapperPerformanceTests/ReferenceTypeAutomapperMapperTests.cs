﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapperPerformance;
using AutoMapperPerformance.Mappers;
using AutoMapperPerformance.Objects;
using NUnit.Framework;

namespace AutoMapperPerformanceTests
{
    [TestFixture]
    public class ReferenceTypeAutomapperMapperTests
    {
        private const int NumberOfInputElements = 1;

        private IList<ReferenceTypeVariablesSmallObjectA> _smallAObjects;
        private ReferenceTypeAutomapperMapper SUT;

        [OneTimeSetUp]
        public void Setup()
        {
            _smallAObjects = ObjectGenerator.GenerateCollectionOfObjects<ReferenceTypeVariablesSmallObjectA>(NumberOfInputElements);
            SUT = new ReferenceTypeAutomapperMapper(_smallAObjects);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            SUT.ResetMapper();
        }

        [Test]
        public void CheckIfCountOfInputElementEqualsOutput([Random(1, 100, 1)] int numberOfInputElements)
        {
            // Assign
            _smallAObjects = ObjectGenerator.GenerateCollectionOfObjects<ReferenceTypeVariablesSmallObjectA>(NumberOfInputElements);

            // Act
            var result = SUT.ToObjectsB();

            // Assert
            Assert.AreEqual(_smallAObjects.Count, result.Count);
        }

        [Test]
        public void CheckIfTypeOfOutputIsCorrect_InputReferenceTypeVariablesSmallObjectA_OutputReferenceTypeVariablesSmallObjectB()
        {
            // Assign
            var _smallAObjects = ObjectGenerator.GenerateCollectionOfObjects<ReferenceTypeVariablesSmallObjectA>(1);

            // Act
            var result = SUT.ToObjectsB();

            // Assert
            Assert.IsNotInstanceOf<List<ReferenceTypeVariablesSmallObjectA>>(result);
            Assert.IsInstanceOf<List<ReferenceTypeVariablesSmallObjectB>>(result);
        }

        [Test]
        public void CheckIfChangeInObjectACauseChangeInObjectB()
        {
            // Assign
            var inputObjects = _smallAObjects;

            // Act
            var result = SUT.ToObjectsB();
            inputObjects[0].MugObject.Name = "New name!";


            // Assert
            Assert.AreEqual(inputObjects[0].MugObject.Name, result.First(o => o.Id == inputObjects[0].Id).MugObject.Name);
        }
    }
}
