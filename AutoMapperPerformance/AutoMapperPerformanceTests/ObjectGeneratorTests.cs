﻿using System;
using System.Collections.Generic;
using AutoMapperPerformance;
using NUnit.Framework;

namespace AutoMapperPerformanceTests
{
    [TestFixture]
    public class ObjectGeneratorTests
    {
        [Test]
        public void GenerateCollectionOfObjects_0Element_OutOfRangeException()
        {
            // Act
            void methodCall() => ObjectGenerator.GenerateCollectionOfObjects<SampleDto>(0);

            // Assert
            Assert.Throws<IndexOutOfRangeException>(methodCall);
        }

        [Test]
        public void GenerateCollectionOfObjects_Minus1Element_OutOfRangeException()
        {
            // Act
            void methodCall() => ObjectGenerator.GenerateCollectionOfObjects<SampleDto>(-1);

            // Assert
            Assert.Throws<IndexOutOfRangeException>(methodCall);
        }

        [Test]
        public void CheckIfGeneratedArrayHaveProperNumberOfElements([Random(0, 100, 1)] int numberOfElement)
        {
            // Act
            var result = ObjectGenerator.GenerateCollectionOfObjects<SampleDto>(numberOfElement);

            // Assert
            Assert.AreEqual(numberOfElement, result.Count);
        }

        [Test]
        public void CheckResultType_string_expectListOfString()
        {
            // Act
            var result = ObjectGenerator.GenerateCollectionOfObjects<SampleDto>(2);

            // Assert
            Assert.IsInstanceOf<List<SampleDto>>(result);
        }
    }
}
