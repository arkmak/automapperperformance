using AutoMapperPerformance.TestCases;
using BenchmarkDotNet.Running;

namespace AutoMapperPerformance
{
    class Program
    {
        static void Main(string[] args)
        {
            //BenchmarkRunner.Run<MixedTypePerformanceTest>();
            BenchmarkRunner.Run<ReferenceTypePerformanceTest>();
            //BenchmarkRunner.Run<ValueTypePerformanceTest>();


            System.Console.ReadLine();
        }
    }
}
