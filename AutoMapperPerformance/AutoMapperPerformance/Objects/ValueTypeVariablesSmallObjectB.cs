﻿namespace AutoMapperPerformance.Objects
{
    public class ValueTypeVariablesSmallObjectB
    {
        public long Id { get; set; }
        public int Number { get; set; }
        public bool IsActive { get; set; }
        public char Char { get; set; }
        public double Double { get; set; }
        public float Float { get; set; }
    }
}
