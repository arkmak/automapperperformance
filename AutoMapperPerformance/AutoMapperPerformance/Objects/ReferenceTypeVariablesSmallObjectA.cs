﻿namespace AutoMapperPerformance.Objects
{
    public class ReferenceTypeVariablesSmallObjectA
    {
        public string Id { get; set; }
        public string Number { get; set; }
        public string IsActive { get; set; }
        public string String1 { get; set; }
        public string String2 { get; set; }
        public string String3 { get; set; }
        public Mug MugObject { get; set; }

        public ReferenceTypeVariablesSmallObjectA()
        {

            MugObject = new Mug
            {
                Name = "456",
                Height = 200,
                Radius = 15
            };
        }
    }
}
