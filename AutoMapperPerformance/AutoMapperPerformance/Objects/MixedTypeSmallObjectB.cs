﻿namespace AutoMapperPerformance
{
    public class MixedTypeSmallObjectB
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Value { get; set; }
    }
}
