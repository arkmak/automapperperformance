﻿namespace AutoMapperPerformance.Objects
{
    public class Mug
    {
        public int Radius { get; set; }

        public int Height { get; set; }

        public string Name { get; set; }
    }
}
