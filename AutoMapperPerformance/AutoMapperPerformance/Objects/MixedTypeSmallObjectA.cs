﻿namespace AutoMapperPerformance
{
    public class MixedTypeSmallObjectA
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Value { get; set; }
    }
}
