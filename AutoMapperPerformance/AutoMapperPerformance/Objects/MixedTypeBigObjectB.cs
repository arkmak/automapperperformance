﻿using System;

namespace AutoMapperPerformance.Objects
{
    public class MixedTypeBigObjectB
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public long CreatingAdviserId { get; set; }
        public long BelongingAdviserId { get; set; }
        public long AdviserId { get; set; }
        public string HeadAgencyName { get; set; }
        public string Acronym { get; set; }
        public string SingleTitle { get; set; }
        public string SingleFirstName { get; set; }
        public string SingleLastName { get; set; }
        public string SingleAddress { get; set; }
        public string SinglePostalCode { get; set; }
        public string SingleCountryOfResidence { get; set; }
        public string SingleNationality { get; set; }
        public DateTime? SingleDateOfBirth { get; set; }
        public string SingleTelephone { get; set; }
        public string SingleMobile { get; set; }
        public string SingleEmail { get; set; }
        public string SinglePreferredLanguage { get; set; }
        public string JointTitle { get; set; }
        public string JointFirstName { get; set; }
        public string JointLastName { get; set; }
        public string JointAddress { get; set; }
        public string JointPostalCode { get; set; }
        public string JointCountryOfResidence { get; set; }
        public string JointNationality { get; set; }
        public DateTime? JointDateOfBirth { get; set; }
        public string JointTelephone { get; set; }
        public string JointMobile { get; set; }
        public string JointEmail { get; set; }
        public string JointPreferredLanguage { get; set; }
        public string CorporateName { get; set; }
        public string CorporateAddress { get; set; }
        public string CorporatePostalCode { get; set; }
        public string CorporateCountryOfResidence { get; set; }
        public string CorporatePreferredLanguage { get; set; }
        public byte ByteArray { get; set; }
        public string LoremIpsum { get; set; }
        public long[] BigLongArray { get; set; }
    }
}
