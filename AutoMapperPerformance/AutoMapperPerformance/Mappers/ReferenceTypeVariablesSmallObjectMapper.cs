﻿using System.Collections.Generic;
using System.Linq;
using AutoMapperPerformance.Objects;

namespace AutoMapperPerformance.Mappers
{
    public class ReferenceTypeVariablesSmallObjectMapper : MapperBase<ReferenceTypeVariablesSmallObjectA, ReferenceTypeVariablesSmallObjectB>
    {
        public ReferenceTypeVariablesSmallObjectMapper(IList<ReferenceTypeVariablesSmallObjectA> objectsA) : base(objectsA)
        {
        }

        public override IList<ReferenceTypeVariablesSmallObjectB> ToObjectsB()
        {
            return ObjectsA.Select(o => new ReferenceTypeVariablesSmallObjectB
            {
                Id = o.Id,
                Number = o.Number,
                IsActive = o.IsActive,
                String1 = o.String1,
                String2 = o.String2,
                String3 = o.String3,
                MugObject = o.MugObject
            }).ToList();
        }
    }
}
