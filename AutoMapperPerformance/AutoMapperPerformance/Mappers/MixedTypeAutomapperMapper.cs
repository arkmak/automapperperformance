﻿using System.Collections.Generic;
using AutoMapper;
using AutoMapperPerformance.Objects;

namespace AutoMapperPerformance.Mappers
{
    public class MixedTypeAutomapperMapper
    {
        public IList<MixedTypeSmallObjectA> SmallAObjects { get; }
        public IList<MixedTypeBigObjectA> BigAObjects { get; }

        public MixedTypeAutomapperMapper(IList<MixedTypeSmallObjectA> smallAObjects, IList<MixedTypeBigObjectA> bigAObjects)
        {
            this.SmallAObjects = smallAObjects;
            this.BigAObjects = bigAObjects;
            Mapper.Initialize(cfg => SetupAutomapper(cfg));
        }

        private void SetupAutomapper(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<MixedTypeSmallObjectA, MixedTypeSmallObjectB>();
            cfg.CreateMap<MixedTypeBigObjectA, MixedTypeBigObjectA>();

        }

        public IList<MixedTypeSmallObjectB> ToSmallObjectsB()
        {
            return Mapper.Map<IList<MixedTypeSmallObjectB>>(SmallAObjects);
        }

        public IList<MixedTypeBigObjectA> ToBigObjectsB()
        {
            return Mapper.Map<IList<MixedTypeBigObjectA>>(BigAObjects);
        }
    }
}
