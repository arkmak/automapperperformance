﻿using System.Collections.Generic;
using System.Linq;
using AutoMapperPerformance.Objects;

namespace AutoMapperPerformance.Mappers
{
    public class ValueTypeVariablesSmallObjectMapper : MapperBase<ValueTypeVariablesSmallObjectA, ValueTypeVariablesSmallObjectB>
    {
        public ValueTypeVariablesSmallObjectMapper(IList<ValueTypeVariablesSmallObjectA> objectsA) : base(objectsA)
        {
        }

        public override IList<ValueTypeVariablesSmallObjectB> ToObjectsB()
        {
            return ObjectsA.Select(o => new ValueTypeVariablesSmallObjectB
            {
                Id = o.Id,
                Number = o.Number,
                Char = o.Char,
                Double = o.Double,
                Float = o.Float,
                IsActive = o.IsActive
            }).ToList();
        }
    }
}
