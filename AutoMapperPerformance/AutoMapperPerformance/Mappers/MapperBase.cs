﻿using System.Collections.Generic;

namespace AutoMapperPerformance.Mappers
{
    public abstract class MapperBase<inputObjects, outputObjects>
    {
        protected IList<inputObjects> ObjectsA;

        public MapperBase(IList<inputObjects> objectsA)
        {
            this.ObjectsA = objectsA;
        }

        public abstract IList<outputObjects> ToObjectsB();
    }
}