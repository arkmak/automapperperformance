﻿using System.Collections.Generic;
using System.Linq;
using AutoMapperPerformance.Mappers;

namespace AutoMapperPerformance
{
    public class MixedTypeSmallObjectMapper : MapperBase<MixedTypeSmallObjectA, MixedTypeSmallObjectB>
    {
        public MixedTypeSmallObjectMapper(IList<MixedTypeSmallObjectA> SmallAObjects) : base(SmallAObjects)
        {

        }

        public override IList<MixedTypeSmallObjectB> ToObjectsB()
        {
            return ObjectsA.Select(o => new MixedTypeSmallObjectB
            {
                Id = o.Id,
                FirstName = o.FirstName,
                LastName = o.LastName,
                Value = o.Value
            }).ToList();
        }
    }
}
