﻿using System.Collections.Generic;
using AutoMapper;
using AutoMapperPerformance.Objects;

namespace AutoMapperPerformance.Mappers
{
    public class ValueTypeAutomapperMapper
    {
        public IList<ValueTypeVariablesSmallObjectA> SmallAObjects { get; }

        public ValueTypeAutomapperMapper(IList<ValueTypeVariablesSmallObjectA> smallAObjects)
        {
            this.SmallAObjects = smallAObjects;
            Mapper.Initialize(cfg => SetupAutomapper(cfg));
        }

        private void SetupAutomapper(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<ValueTypeVariablesSmallObjectA, ValueTypeVariablesSmallObjectB>();
        }
        public IList<ValueTypeVariablesSmallObjectB> ToObjectsB()
        {
            return Mapper.Map<IList<ValueTypeVariablesSmallObjectB>>(SmallAObjects);
        }

        public void ResetMapper()
        {
            Mapper.Reset();
        }
    }
}
