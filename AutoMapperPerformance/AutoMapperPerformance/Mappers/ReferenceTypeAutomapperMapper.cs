﻿using System.Collections.Generic;
using AutoMapper;
using AutoMapperPerformance.Objects;

namespace AutoMapperPerformance.Mappers
{
    public class ReferenceTypeAutomapperMapper
    {
        public IList<ReferenceTypeVariablesSmallObjectA> SmallAObjects { get; }

        public ReferenceTypeAutomapperMapper(IList<ReferenceTypeVariablesSmallObjectA> smallAObjects)
        {
            this.SmallAObjects = smallAObjects;
            Mapper.Initialize(cfg => SetupAutomapper(cfg));
        }

        private void SetupAutomapper(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<ReferenceTypeVariablesSmallObjectA, ReferenceTypeVariablesSmallObjectB>();
        }
        public IList<ReferenceTypeVariablesSmallObjectB> ToObjectsB()
        {
            return Mapper.Map<IList<ReferenceTypeVariablesSmallObjectB>>(SmallAObjects);
        }

        public void ResetMapper()
        {
            Mapper.Reset();
        }
    }
}
