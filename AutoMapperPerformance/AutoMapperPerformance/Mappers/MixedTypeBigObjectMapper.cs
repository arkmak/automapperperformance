﻿using System.Collections.Generic;
using System.Linq;
using AutoMapperPerformance.Objects;

namespace AutoMapperPerformance.Mappers
{
    public class MixedTypeBigObjectMapper : MapperBase<MixedTypeBigObjectA, MixedTypeBigObjectB>
    {
        public MixedTypeBigObjectMapper(IList<MixedTypeBigObjectA> SmallAObjects) : base(SmallAObjects)
        {

        }

        public override IList<MixedTypeBigObjectB> ToObjectsB()
        {
            return ObjectsA.Select(o => new MixedTypeBigObjectB
            {
                Acronym = o.Acronym,
                AdviserId = o.AdviserId,
                BelongingAdviserId = o.BelongingAdviserId,
                ByteArray = o.ByteArray,
                CorporateAddress = o.CorporateAddress,
                CorporateCountryOfResidence = o.CorporateCountryOfResidence,
                CorporateName = o.CorporateName,
                CorporatePostalCode = o.CorporatePostalCode,
                CorporatePreferredLanguage = o.CorporatePreferredLanguage,
                CreatingAdviserId = o.CreatingAdviserId,
                HeadAgencyName = o.HeadAgencyName,
                JointAddress = o.JointAddress,
                JointCountryOfResidence = o.JointCountryOfResidence,
                JointDateOfBirth = o.JointDateOfBirth,
                JointEmail = o.JointEmail,
                JointFirstName = o.JointFirstName,
                JointLastName = o.JointLastName,
                JointMobile = o.JointMobile,
                JointNationality = o.JointNationality,
                JointPostalCode = o.JointPostalCode,
                JointPreferredLanguage = o.JointPreferredLanguage,
                JointTelephone = o.JointTelephone,
                JointTitle = o.JointTitle,
                LoremIpsum = o.LoremIpsum,
                SingleAddress = o.SingleAddress,
                SingleCountryOfResidence = o.SingleCountryOfResidence,
                SingleDateOfBirth = o.SingleDateOfBirth,
                SingleEmail = o.SingleEmail,
                SingleFirstName = o.SingleFirstName,
                SingleLastName = o.SingleLastName,
                SingleMobile = o.SingleMobile,
                SingleNationality = o.SingleNationality,
                SinglePostalCode = o.SinglePostalCode,
                SinglePreferredLanguage = o.SinglePreferredLanguage,
                SingleTelephone = o.SingleTelephone,
                SingleTitle = o.SingleTitle,
                UserId = o.UserId,
                UserName = o.UserName
            }).ToList();
        }
    }
}
