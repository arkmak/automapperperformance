﻿using System;
using System.Collections.Generic;
using FizzWare.NBuilder;

namespace AutoMapperPerformance
{
    public static class ObjectGenerator
    {
        public static IList<T> GenerateCollectionOfObjects<T>(int nrOfObjects = 100000)
        {
            if (nrOfObjects > 0)
            {
                return Builder<T>.CreateListOfSize(nrOfObjects).Build();
            }

            throw new IndexOutOfRangeException("Out can generate collection with at least 1 element");
        }
    }
}
