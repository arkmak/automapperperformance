﻿using System.Collections.Generic;
using AutoMapperPerformance.Mappers;
using AutoMapperPerformance.Objects;
using BenchmarkDotNet.Attributes;

namespace AutoMapperPerformance.TestCases
{
    public class MixedTypePerformanceTest
    {
        private IList<MixedTypeSmallObjectA> _smallAObjects;
        private IList<MixedTypeBigObjectA> _bigObjects;
        private MixedTypeSmallObjectMapper smallObjectMapper;
        private MixedTypeBigObjectMapper bigObjectMapper;
        private MixedTypeAutomapperMapper automapperTest;

        [GlobalSetup]
        public void Setup()
        {
            _smallAObjects = ObjectGenerator.GenerateCollectionOfObjects<MixedTypeSmallObjectA>();
            _bigObjects = ObjectGenerator.GenerateCollectionOfObjects<MixedTypeBigObjectA>();

            smallObjectMapper = new MixedTypeSmallObjectMapper(_smallAObjects);
            bigObjectMapper = new MixedTypeBigObjectMapper(_bigObjects);

            automapperTest = new MixedTypeAutomapperMapper(_smallAObjects, _bigObjects);
        }

        [Benchmark]
        [BenchmarkCategory("SmallObject")]
        public void AutomapperTestSmallObject()
        {
            automapperTest.ToSmallObjectsB();
        }

        [Benchmark]
        [BenchmarkCategory("SmallObject")]
        public void ManuallMappTestSmallObject()
        {
            smallObjectMapper.ToObjectsB();
        }

        [Benchmark]
        [BenchmarkCategory("BigObject")]
        public void AutomapperTestBigObject()
        {
            automapperTest.ToBigObjectsB();
        }

        [Benchmark]
        [BenchmarkCategory("BigObject")]
        public void ManuallMappTestBigObject()
        {
            bigObjectMapper.ToObjectsB();
        }

    }
}
