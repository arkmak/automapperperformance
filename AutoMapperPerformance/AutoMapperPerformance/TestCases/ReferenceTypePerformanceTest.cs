﻿using AutoMapperPerformance.Mappers;
using AutoMapperPerformance.Objects;
using BenchmarkDotNet.Attributes;

namespace AutoMapperPerformance.TestCases
{
    public class ReferenceTypePerformanceTest
    {
        private ReferenceTypeVariablesSmallObjectMapper manualMappTest;
        private ReferenceTypeAutomapperMapper automapperTest;

        [GlobalSetup]
        public void Setup()
        {
            var _smallAObjects = ObjectGenerator.GenerateCollectionOfObjects<ReferenceTypeVariablesSmallObjectA>();

            manualMappTest = new ReferenceTypeVariablesSmallObjectMapper(_smallAObjects);

            automapperTest = new ReferenceTypeAutomapperMapper(_smallAObjects);
        }

        [Benchmark]
        [BenchmarkCategory("SmallObject")]
        public void AutomapperTestSmallObject()
        {
            automapperTest.ToObjectsB();
        }

        [Benchmark]
        [BenchmarkCategory("SmallObject")]
        public void ManuallMappTestSmallObject()
        {
            manualMappTest.ToObjectsB();
        }
    }
}
