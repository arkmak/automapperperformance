﻿using AutoMapperPerformance.Mappers;
using AutoMapperPerformance.Objects;
using BenchmarkDotNet.Attributes;

namespace AutoMapperPerformance.TestCases
{
    public class ValueTypePerformanceTest
    {
        private ValueTypeVariablesSmallObjectMapper manualMappTest;
        private ValueTypeAutomapperMapper automapperTest;

        [GlobalSetup]
        public void Setup()
        {
            var _smallAObjects = ObjectGenerator.GenerateCollectionOfObjects<ValueTypeVariablesSmallObjectA>();

            manualMappTest = new ValueTypeVariablesSmallObjectMapper(_smallAObjects);

            automapperTest = new ValueTypeAutomapperMapper(_smallAObjects);
        }

        [Benchmark]
        [BenchmarkCategory("SmallObject")]
        public void AutomapperTestSmallObject()
        {
            automapperTest.ToObjectsB();
        }

        [Benchmark]
        [BenchmarkCategory("SmallObject")]
        public void ManuallMappTestSmallObject()
        {
            manualMappTest.ToObjectsB();
        }
    }
}
